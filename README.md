# LCD touchscreen subsystem in FPGA

Subsystem for LCD touchscreen implemented on FPGA MAXimator platform using NIOS II soft-core processor architecture.
This project is an engineering thesis at Department of Computer Science, Electronics and Telecommunication of AGH UST in Cracow.

## Assumptions:
* SPI video signal generator
    * maximum resolution - 320x240
    * color depth - RGB565
* software-based VRAM organisation and handling (VRAM in display module)
* touchpanel handled with SPI
* demo application design (Paint-like)

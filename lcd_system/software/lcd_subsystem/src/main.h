/*
 * main.h
 *
 *  Created on: 9 dec 2020
 *      Author: �ukasz Bielecki
 */

#ifndef SRC_MAIN_H_
#define SRC_MAIN_H_

//#include "system.h"
#include "sys/alt_stdio.h"
#include "sys/alt_sys_wrappers.h"
//#include "altera_avalon_pio_regs.h"
//#include "altera_avalon_spi.h"
#include "altera_avalon_timer_regs.h"
#include "sys/alt_irq.h"
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
//#include <string.h>

#include "gui_system.h"

void keys_interrupt(void* context);
void debouncer_timer_interrupt(void* context);
void ts_timer_interrupt(void* context[]);
void reset_timer(uint8_t* base);

#endif /* SRC_MAIN_H_ */

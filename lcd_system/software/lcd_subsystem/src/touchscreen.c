/*
 * touchscreen.c
 *
 *  Created on: 9 dec 2020
 *      Author: �ukasz Bielecki
 */

#include "touchscreen.h"


uint8_t read_ts(pixel_t* coords_read)
{
	uint16_t ts_command[1];
	pixel_t temp_pixel;
	static pixel_t previous_pixel;
	static uint32_t x_filter_reg;
	static uint32_t y_filter_reg;
	static uint8_t first_read;
	static uint8_t permit_read;

	if(TS_INT)	// check if screen is touched
	{
		first_read = 1;
		return NOTOUCH;
	}
	else
	{
		if(first_read)
		{
			first_read = 0;
			ALT_USLEEP(10000);
		}
		ts_command[0] = READ_TS_X;
		alt_avalon_spi_command(SPI_BASE, 0, 1, ts_command, 1, (uint16_t*)&(temp_pixel.X), 0);
		temp_pixel.X = ((temp_pixel.X >> 3) ^ 0x0FFF);	// get 12 bits of data and do not operation, so untouched is 0

		ts_command[0] = READ_TS_Y;
		alt_avalon_spi_command(SPI_BASE, 0, 1, ts_command, 1, (uint16_t*)&(temp_pixel.Y), 0);
		temp_pixel.Y = ((temp_pixel.Y >> 3));	// get 12 bits of data

		previous_pixel = temp_pixel;

		if((abs(temp_pixel.X - previous_pixel.X) < MAX_OFFSET) && (abs(temp_pixel.Y - previous_pixel.Y) < MAX_OFFSET))
		{
			*coords_read = temp_pixel;
			return VALID;
		}
		else
		{
			return ERROR;
		}
	}
}

void filter_data(uint16_t* input, uint16_t* output, uint32_t* filter_reg)
{
	*filter_reg = *filter_reg - (*filter_reg >> FILTER_SHIFT) + *input;
	*output = (*filter_reg >> FILTER_SHIFT);
}


/*
 * system.c
 *
 *  Created on: 16 dec 2020
 *      Author: Lukasz Bielecki
 */

#include "gui_system.h"

shape_t draw_shape = RECTANGLE;
uint8_t draw_fill = ERROR;
uint8_t draw_red = 0x1F;
uint8_t draw_green = 0;
uint8_t draw_blue = 0;
uint16_t draw_color = COLOR_RED;

const color_set_t colors_basic = {.background = COLOR_WHITE, .border = COLOR_BLACK, .button = COLOR_GREEN,
									.touched_button = COLOR_BLUE, .active_button = COLOR_RED, .text = COLOR_BLACK};
//const pixel_t basic_start_point = {250, 0};
top_menu_elem_t top_menu = {NULL, NULL, {0,0}, {320, 40}, &colors_basic, NULL, NULL};
top_menu_elem_t color_menu = {NULL, NULL, {10,50}, {300, 180}, &colors_basic, NULL, NULL};
top_menu_elem_t shape_menu = {NULL, NULL, {10,50}, {300, 180}, &colors_basic, NULL, NULL};

basic_menu_elem_t button_color = {.rel_start_point = {5, 0}, .dimensions = {100, 40}, .text = "Color",
									.prev = NULL, .next = NULL, .draw_cb = &draw_button, .focus_cb = &focus_button,
									.unfocus_cb = &draw_button, .activity_cb = &activate_color, .deactivate_cb = &deactivate_color};

basic_menu_elem_t button_shape = {.rel_start_point = {110, 0}, .dimensions = {100, 40}, .text = "Shape",
									.prev = NULL, .next = NULL, .draw_cb = &draw_button, .focus_cb = &focus_button,
									.unfocus_cb = &draw_button, .activity_cb = &activate_shape, .deactivate_cb = &deactivate_shape};

basic_menu_elem_t button_exit = {.rel_start_point = {320-105, 0}, .dimensions = {100, 40}, .text = "Exit",
									.prev = NULL, .next = NULL, .draw_cb = &draw_button, .focus_cb = &focus_button,
									.unfocus_cb = &draw_button, .activity_cb = &erase_menu, .deactivate_cb = NULL};


basic_menu_elem_t list_shape_rect = {.rel_start_point = {20, 140}, .dimensions = {10, 10}, .text = "Rectangle",
									.prev = NULL, .next = NULL, .draw_cb = &draw_list_elem, .focus_cb = &focus_list_elem,
									.unfocus_cb = NULL, .activity_cb = NULL, .deactivate_cb = NULL};

basic_menu_elem_t list_shape_circle = {.rel_start_point = {20, 100}, .dimensions = {10, 10}, .text = "Circle",
									.prev = NULL, .next = NULL, .draw_cb = &draw_list_elem, .focus_cb = &focus_list_elem,
									.unfocus_cb = NULL, .activity_cb = NULL, .deactivate_cb = NULL};


basic_menu_elem_t checkbox_fill = {.rel_start_point = {17, 30}, .dimensions = {16, 16}, .text = "Fill",
									.prev = NULL, .next = NULL, .draw_cb = &draw_fill_checkbox, .focus_cb = NULL,
									.unfocus_cb = NULL, .activity_cb = &click_fill_checkbox, .deactivate_cb = &click_fill_checkbox};

basic_menu_elem_t slider_red = {.rel_start_point = {26, 100}, .dimensions = {SLIDER_LENGTH+SLIDER_OFFSET, 6}, .text = "Red",
									.prev = NULL, .next = NULL, .draw_cb = &draw_slider_elem, .focus_cb = &focus_slider_elem,
									.unfocus_cb = NULL, .activity_cb = NULL, .deactivate_cb = NULL};

basic_menu_elem_t slider_green = {.rel_start_point = {26, 60}, .dimensions = {SLIDER_LENGTH+SLIDER_OFFSET, 6}, .text = "Green",
									.prev = NULL, .next = NULL, .draw_cb = &draw_slider_elem, .focus_cb = &focus_slider_elem,
									.unfocus_cb = NULL, .activity_cb = NULL, .deactivate_cb = NULL};

basic_menu_elem_t slider_blue = {.rel_start_point = {26, 20}, .dimensions = {SLIDER_LENGTH+SLIDER_OFFSET, 6}, .text = "Blue",
									.prev = NULL, .next = NULL, .draw_cb = &draw_slider_elem, .focus_cb = &focus_slider_elem,
									.unfocus_cb = NULL, .activity_cb = NULL, .deactivate_cb = NULL};

basic_menu_elem_t color_probe = {.rel_start_point = {120, 145}, .dimensions = {60, 30}, .text = "Probe",
									.prev = NULL, .next = NULL, .draw_cb = &draw_color_probe, .focus_cb = NULL,
									.unfocus_cb = NULL, .activity_cb = NULL, .deactivate_cb = NULL};

static top_menu_elem_t* top_head_ptr = NULL;
uint8_t is_active = ERROR;

void init_gui(void)
{
	draw_color = merge_rgb(draw_red, draw_green, draw_blue);
	top_head_ptr = &top_menu;

	add_basic_elem(&button_color, &top_menu);
	add_basic_elem(&button_shape, &top_menu);
	add_basic_elem(&button_exit, &top_menu);

	add_basic_elem(&list_shape_circle, &shape_menu);
	add_basic_elem(&list_shape_rect, &shape_menu);
	add_basic_elem(&checkbox_fill, &shape_menu);

	add_basic_elem(&slider_red, &color_menu);
	add_basic_elem(&slider_green, &color_menu);
	add_basic_elem(&slider_blue, &color_menu);
	add_basic_elem(&color_probe, &color_menu);
}

void add_basic_elem(basic_menu_elem_t* element, top_menu_elem_t* top)
{
    basic_menu_elem_t* pass_ptr = top->head_ptr;
    if(NULL == top->head_ptr)
    {
        top->head_ptr = element;
        element->prev = NULL;
        element->next = NULL;
    }
    else
    {
        while(NULL != pass_ptr->next)
        {
            pass_ptr = pass_ptr->next;
        }
        pass_ptr->next = element;
        element->prev = pass_ptr;
        element->next = NULL;
    }
}

uint8_t delete_basic_elem(size_t element, top_menu_elem_t* top)
{
    basic_menu_elem_t* pass_ptr = top->head_ptr;
    if(0 == element)
    {
        pass_ptr->next->prev = NULL;
        top->head_ptr = pass_ptr->next;
        return VALID;
    }
    else
    {
        for(int i=0;i<element;++i)
        {
            pass_ptr = pass_ptr->next;
            if(NULL == pass_ptr)
            {
                return ERROR;
            }
        }
        pass_ptr->prev->next = pass_ptr->next;
        if(NULL != pass_ptr->next)
        {
            pass_ptr->next->prev = pass_ptr->prev;
        }
        return VALID;
    }
    return ERROR;
}

void add_top_elem(top_menu_elem_t* element)
{
    top_menu_elem_t* pass_ptr = top_head_ptr;
    if(NULL == top_head_ptr)
    {
        top_head_ptr = element;
        element->prev = NULL;
        element->next = NULL;
    }
    else
    {
        while(NULL != pass_ptr->next)
        {
            pass_ptr = pass_ptr->next;
        }
        pass_ptr->next = element;
        element->prev = pass_ptr;
        element->next = NULL;
    }
    draw_menu(element);
}

// deletes last element in the top list
uint8_t delete_top_elem(void)
{
    top_menu_elem_t* pass_ptr = top_head_ptr;
    if(NULL == top_head_ptr || NULL == top_head_ptr->next)
    {
    	return ERROR;
    }
    while ((pass_ptr->next)->next != NULL)
    {
    	pass_ptr = pass_ptr->next;
    }
    draw_filled_rectangle(&(pass_ptr->next->start_point), pass_ptr->next->dimensions[0], pass_ptr->next->dimensions[1], COLOR_BG);
    pass_ptr->next = NULL;

    return VALID;
}

size_t draw_menu(top_menu_elem_t* top)
{
	// if top is a null pointer, draw basic menu
	if(NULL == top)
	{
		top = top_head_ptr;
	}
	basic_menu_elem_t* pass_ptr = top->head_ptr;
    size_t number_of_elems = 0;
    top->active_elem_ptr = NULL;
    draw_framed_rectangle(&(top->start_point), top->dimensions[0], top->dimensions[1], top->colors->border, top->colors->background);
    if(NULL != top->head_ptr)
    {
        do
        {
           // use callback funtion if it is defined
        	if(NULL != pass_ptr->draw_cb)
        	{
        		(*(pass_ptr->draw_cb))(pass_ptr, top);
        		++number_of_elems;
        	}
        	pass_ptr = pass_ptr->next;
        }while(NULL != pass_ptr);
        is_active = VALID;
        return number_of_elems;
    }
    return 0;
}

void erase_menu(void)
{
	if(NULL == top_head_ptr)
	{
		return;
	}
	top_menu_elem_t* top_pass_ptr = top_head_ptr;

	// loop iterating through top elements list
	do
	{
		draw_filled_rectangle(&(top_pass_ptr->start_point), top_pass_ptr->dimensions[0], top_pass_ptr->dimensions[1], COLOR_BG);
		top_pass_ptr = top_pass_ptr->next;
	}while(NULL !=top_pass_ptr);

	top_head_ptr->next = NULL;
    is_active = ERROR;
}

void draw_button(basic_menu_elem_t* element, top_menu_elem_t* top)
{
	pixel_t draw_point = {top->start_point.X + element->rel_start_point.X, top->start_point.Y + element->rel_start_point.Y};
	draw_framed_rectangle(&draw_point, element->dimensions[0], element->dimensions[1], top->colors->border, top->colors->button);
	draw_point.X += 50 - strlen(element->text)*3;
	draw_point.Y += 28;
	TFT_Text(&draw_point, top->colors->text, top->colors->button, element->text);
}

void focus_button(basic_menu_elem_t* element, top_menu_elem_t* top)
{
	pixel_t draw_point = {top->start_point.X + element->rel_start_point.X, top->start_point.Y + element->rel_start_point.Y};
	draw_framed_rectangle(&draw_point, element->dimensions[0], element->dimensions[1], top->colors->border, top->colors->touched_button);
	draw_point.X += 50 - strlen(element->text)*3;
	draw_point.Y += 28;
	TFT_Text(&draw_point, top->colors->text, top->colors->touched_button, element->text);
}

void activate_color(basic_menu_elem_t* element, top_menu_elem_t* top)
{
	pixel_t draw_point = {top->start_point.X + element->rel_start_point.X, top->start_point.Y + element->rel_start_point.Y};
	draw_framed_rectangle(&draw_point, element->dimensions[0], element->dimensions[1], top->colors->border, top->colors->active_button);
	draw_point.X += 50 - strlen(element->text)*3;
	draw_point.Y += 28;
	TFT_Text(&draw_point, top->colors->text, top->colors->active_button, element->text);
	add_top_elem(&color_menu);
}

void activate_shape(basic_menu_elem_t* element, top_menu_elem_t* top)
{
	pixel_t draw_point = {top->start_point.X + element->rel_start_point.X, top->start_point.Y + element->rel_start_point.Y};
	draw_framed_rectangle(&draw_point, element->dimensions[0], element->dimensions[1], top->colors->border, top->colors->active_button);
	draw_point.X += 50 - strlen(element->text)*3;
	draw_point.Y += 28;
	TFT_Text(&draw_point, top->colors->text, top->colors->active_button, element->text);
	add_top_elem(&shape_menu);
}

void deactivate_color(basic_menu_elem_t* element, top_menu_elem_t* top)
{
	(*(element->draw_cb))(element, top);
	delete_top_elem();
}

void deactivate_shape(basic_menu_elem_t* element, top_menu_elem_t* top)
{
	(*(element->draw_cb))(element, top);
	delete_top_elem();
}

void draw_list_elem(basic_menu_elem_t* element, top_menu_elem_t* top)
{
	pixel_t draw_point = {top->start_point.X + element->rel_start_point.X + (element->dimensions[0])/2,
							top->start_point.Y + element->rel_start_point.Y + (element->dimensions[1])/2};
	if(VALID == strcmp(element->text,"Circle"))
	{
		if(CIRCLE == draw_shape)
		{
			draw_filled_circle(&draw_point, element->dimensions[1], top->colors->border);
		}
		else
		{
			draw_filled_circle(&draw_point, element->dimensions[1], top->colors->background);
			draw_circle(&draw_point, element->dimensions[1], top->colors->border);
		}
	}
	else if(VALID == strcmp(element->text,"Rectangle"))
	{
		if(RECTANGLE == draw_shape)
		{
			draw_filled_circle(&draw_point, element->dimensions[1], top->colors->border);
		}
		else
		{
			draw_filled_circle(&draw_point, element->dimensions[1], top->colors->background);
			draw_circle(&draw_point, element->dimensions[1], top->colors->border);
		}
	}
	draw_point.X += element->dimensions[0]/2+10;
	draw_point.Y += element->dimensions[1];
	TFT_Text(&draw_point, top->colors->text, top->colors->background, element->text);

}

void focus_list_elem(basic_menu_elem_t* element, top_menu_elem_t* top)
{

	if(VALID == strcmp(element->text,"Circle"))
	{
		if(CIRCLE != draw_shape)
		{
			draw_shape = CIRCLE;
			draw_list_elem(&list_shape_rect, top);
			draw_list_elem(element, top);
		}
	}
	else if(VALID == strcmp(element->text,"Rectangle"))
	{
		if(RECTANGLE != draw_shape)
		{
			draw_shape = RECTANGLE;
			draw_list_elem(element, top);
			draw_list_elem(&list_shape_circle, top);
		}
	}
}

void draw_fill_checkbox(basic_menu_elem_t* element, top_menu_elem_t* top)
{
	pixel_t draw_point = {top->start_point.X + element->rel_start_point.X, top->start_point.Y + element->rel_start_point.Y};

	if(VALID == draw_fill)
	{
		draw_filled_rectangle(&draw_point, element->dimensions[0], element->dimensions[1], top->colors->border);
	}
	else
	{
		draw_framed_rectangle(&draw_point, element->dimensions[0], element->dimensions[1], top->colors->border, top->colors->background);
	}
	draw_point.X += element->dimensions[0]+5;
	draw_point.Y += element->dimensions[1];
	TFT_Text(&draw_point, top->colors->text, top->colors->background, element->text);
}

void click_fill_checkbox(basic_menu_elem_t* element, top_menu_elem_t* top)
{
	if(VALID == draw_fill)
	{
		draw_fill = ERROR;
	}
	else
	{
		draw_fill = VALID;
	}
	draw_fill_checkbox(element, top);
}

void draw_slider_elem(basic_menu_elem_t* element, top_menu_elem_t* top)
{
	uint8_t max_color;
	uint8_t* current_color;
	if(VALID == strcmp("Red", element->text))
	{
		max_color = RED_MAX;
		current_color = &draw_red;
	}
	else if(VALID == strcmp("Green", element->text))
	{
		max_color = GREEN_MAX;
		current_color = &draw_green;
	}
	else if(VALID == strcmp("Blue", element->text))
	{
		max_color = BLUE_MAX;
		current_color = &draw_blue;
	}
	// unknown color
	else
	{
		return;
	}
	pixel_t draw_point = {top->start_point.X + element->rel_start_point.X, top->start_point.Y + element->rel_start_point.Y};
	draw_filled_rectangle(&draw_point, element->dimensions[0]-SLIDER_OFFSET, element->dimensions[1], top->colors->border);
	draw_point.X += map_rgb_to_slider(*current_color, max_color, SLIDER_LENGTH)-SLIDER_W/2;
	draw_point.Y += (element->dimensions[1]/2 - SLIDER_H/2);
	draw_filled_rectangle(&draw_point, SLIDER_W, SLIDER_H, top->colors->border);
	draw_point.X = top->start_point.X + element->rel_start_point.X;
	draw_point.Y = top->start_point.Y + element->rel_start_point.Y + SLIDER_H*3/2;
	TFT_Text(&draw_point, top->colors->text, top->colors->background, element->text);
}

void focus_slider_elem(basic_menu_elem_t* element, top_menu_elem_t* top, pixel_t* position)
{
	uint8_t new_color;
	uint8_t max_color;
	uint8_t* current_color;
	pixel_t draw_point;

	if(VALID == strcmp("Red", element->text))
	{
		max_color = RED_MAX;
		current_color = &draw_red;
	}
	else if(VALID == strcmp("Green", element->text))
	{
		max_color = GREEN_MAX;
		current_color = &draw_green;
	}
	else if(VALID == strcmp("Blue", element->text))
	{
		max_color = BLUE_MAX;
		current_color = &draw_blue;
	}
	// unknown color
	else
	{
		return;
	}

	new_color = map_slider_to_rgb(position->X - element->rel_start_point.X, max_color, SLIDER_LENGTH);

	if(new_color != *current_color)
	{
		// erase old position
		draw_point.X = top->start_point.X + element->rel_start_point.X;
		draw_point.Y = top->start_point.Y + element->rel_start_point.Y;
		draw_point.X += map_rgb_to_slider(*current_color, max_color, SLIDER_LENGTH)-SLIDER_W/2;
		draw_point.Y += (element->dimensions[1]/2 - SLIDER_H/2);
		draw_filled_rectangle(&draw_point, SLIDER_W, SLIDER_H, top->colors->background);
		draw_point.Y -= (element->dimensions[1]/2 - SLIDER_H/2);
		draw_filled_rectangle(&draw_point, SLIDER_W, element->dimensions[1], top->colors->border);

		// change color and draw new position
		*current_color = new_color;
		draw_point.X = top->start_point.X + element->rel_start_point.X;
		draw_point.Y = top->start_point.Y + element->rel_start_point.Y;
		draw_point.X += map_rgb_to_slider(*current_color, max_color, SLIDER_LENGTH)-SLIDER_W/2;
		draw_point.Y += (element->dimensions[1]/2 - SLIDER_H/2);
		draw_filled_rectangle(&draw_point, SLIDER_W, SLIDER_H, top->colors->border);
		draw_color = merge_rgb(draw_red, draw_green, draw_blue);
		draw_color_probe(&color_probe, top);
	}

}

void draw_color_probe(basic_menu_elem_t* element, top_menu_elem_t* top)
{
	pixel_t draw_point = {top->start_point.X + element->rel_start_point.X, top->start_point.Y + element->rel_start_point.Y};
	draw_framed_rectangle(&draw_point, element->dimensions[0], element->dimensions[1], top->colors->border, draw_color);
}

// function checks if given point lies within specified rectangle, borders excluded
uint8_t is_point_within(pixel_t* point, pixel_t* start_point, uint16_t width, uint16_t height)
{
	if((point->X > start_point->X) && (point->X < (start_point->X + width - 1)))
	{
		if((point->Y > start_point->Y) && (point->Y < (start_point->Y + height - 1)))
		{
			return VALID;
		}
	}
	return ERROR;
}

void process_touch(pixel_t* touch_coords, uint8_t* touch_status)
{
	if(NULL == top_head_ptr)
	{
		return;
	}
	pixel_t rel_touch_coords;
	top_menu_elem_t* top_pass_ptr = top_head_ptr;
	basic_menu_elem_t* pass_ptr = top_head_ptr->head_ptr;
	static basic_menu_elem_t* last_focus_ptr;
	static top_menu_elem_t* last_top_focus_ptr;
	if(VALID == is_menu_active())
	{
		// loop iterating through top elements list
		do
		{
			pass_ptr = top_pass_ptr->head_ptr;
			if(VALID == is_point_within(touch_coords, &(top_pass_ptr->start_point), top_pass_ptr->dimensions[0], top_pass_ptr->dimensions[1]))
			{
				rel_touch_coords.X = touch_coords->X - top_pass_ptr->start_point.X;
				rel_touch_coords.Y = touch_coords->Y - top_pass_ptr->start_point.Y;
				if(NULL != top_head_ptr->head_ptr)
				{
					// loop iterating through basic elements list
					do
					{
						if(VALID == is_point_within(&rel_touch_coords, &(pass_ptr->rel_start_point), pass_ptr->dimensions[0], pass_ptr->dimensions[1]))
						{
							// process continous touch
							if(VALID == *touch_status)
							{
								// new element is focused
								if((top_pass_ptr != last_top_focus_ptr || pass_ptr != last_focus_ptr) && NULL != pass_ptr->focus_cb)
								{
									// remove last focus
									if(NULL != last_focus_ptr && NULL != last_focus_ptr->unfocus_cb)
									{
										(*(last_focus_ptr->unfocus_cb))(last_focus_ptr, last_top_focus_ptr);
									}
									if(pass_ptr != top_pass_ptr->active_elem_ptr)
									{
										(*(pass_ptr->focus_cb))(pass_ptr, top_pass_ptr, &rel_touch_coords);
										last_focus_ptr = pass_ptr;
										last_top_focus_ptr = top_pass_ptr;
									}
								}
							}
							// process click
							else if(CLICK == *touch_status)
							{
								// new element is clicked
								if(NULL != pass_ptr->activity_cb)
								{
									// remove last focus
									if(NULL != last_focus_ptr && pass_ptr != last_focus_ptr && NULL != last_focus_ptr->unfocus_cb)
									{
										(*(last_focus_ptr->unfocus_cb))(last_focus_ptr, last_top_focus_ptr);
									}
									last_focus_ptr = NULL;

									if(pass_ptr != top_pass_ptr->active_elem_ptr)
									{
										// remove last active element
										if(NULL != top_pass_ptr->active_elem_ptr && NULL !=top_pass_ptr->active_elem_ptr->deactivate_cb)
										{
											(*(top_pass_ptr->active_elem_ptr->deactivate_cb))(top_pass_ptr->active_elem_ptr, top_pass_ptr);
										}
										(*(pass_ptr->activity_cb))(pass_ptr, top_pass_ptr, &rel_touch_coords);
										top_pass_ptr->active_elem_ptr = pass_ptr;
									}
									else if(NULL != pass_ptr->deactivate_cb)
									{
										(*(pass_ptr->deactivate_cb))(pass_ptr, top_pass_ptr, &rel_touch_coords);
										top_pass_ptr->active_elem_ptr = NULL;
									}	// active_elem_ptr
								}	// activity_cb
							}	// touch_status
							return;
						}	// is_point_within
						pass_ptr = pass_ptr->next;
					}while(NULL != pass_ptr);
				}
			}
			top_pass_ptr = top_pass_ptr->next;
		}while(NULL !=top_pass_ptr);

		// remove last focus if no element is touched
		if(NULL != last_focus_ptr)
		{
			if(NULL != last_focus_ptr->unfocus_cb)
			{
				(*(last_focus_ptr->unfocus_cb))(last_focus_ptr, last_top_focus_ptr);
			}
			last_focus_ptr = NULL;
		}
	}
	// menu is inactive
	else if(VALID == *touch_status)
	{
		if(VALID == draw_fill)
		{
			if(RECTANGLE == draw_shape)
			{
				draw_filled_rectangle(touch_coords, 4, 4, draw_color);
			}
			else if(CIRCLE == draw_shape)
			{
				draw_filled_circle(touch_coords, 4, draw_color);
			}
		}
		else
		{
			if(RECTANGLE == draw_shape)
			{
				draw_rectangle(touch_coords, 4, 4, draw_color);
			}
			else if(CIRCLE == draw_shape)
			{
				draw_circle(touch_coords, 4, draw_color);
			}
		}
	}
}

uint8_t is_menu_active(void)
{
	return is_active;
}

uint16_t merge_rgb(uint8_t red, uint8_t green, uint8_t blue)
{
	uint16_t color = (red & 0x1F) << 11;
	color += (green & 0x3F) << 5;
	color += (blue & 0x1F) << 0;
	return color;
}

uint16_t map_rgb_to_slider(uint8_t color, uint8_t color_max, uint16_t slider_length)
{
	if(color > color_max)
	{
		color = color_max;
	}
	return (color*(slider_length/color_max));
}

uint8_t map_slider_to_rgb(uint16_t position, uint8_t color_max, uint16_t slider_length)
{
	if(position > slider_length)
	{
		position = slider_length;
	}
	return ((position*color_max)/slider_length);
}

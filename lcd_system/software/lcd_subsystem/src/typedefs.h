/*
 * typedefs.h
 *
 *  Created on: 16 dec 2020
 *      Author: Lukasz Bielecki
 */

#ifndef SRC_TYPEDEFS_H_
#define SRC_TYPEDEFS_H_

typedef struct pixel_t
{
	int16_t X;
	int16_t Y;
} pixel_t;

typedef struct keys_t
{
	uint8_t pre_debounce;
	uint8_t pressed;
} keys_t;

typedef struct color_set_t
{
	uint16_t background;
	uint16_t border;
	uint16_t button;
	uint16_t touched_button;
	uint16_t active_button;
	uint16_t text;
} color_set_t;

typedef struct basic_menu_elem_t
{
	pixel_t rel_start_point;
	const uint16_t dimensions[2];
	char* text;
	struct basic_menu_elem_t* prev;
	struct basic_menu_elem_t* next;
	void (*draw_cb)();
	void (*focus_cb)();
	void (*unfocus_cb)();
	void (*activity_cb)();
	void (*deactivate_cb)();
} basic_menu_elem_t;

typedef struct top_menu_elem_t
{
	basic_menu_elem_t* head_ptr;
	basic_menu_elem_t* active_elem_ptr;
	const pixel_t start_point;
	const uint16_t dimensions[2];
	const color_set_t* colors;
	struct top_menu_elem_t* prev;
	struct top_menu_elem_t* next;
} top_menu_elem_t;

typedef struct calibration_matrix_t
{
	float A;
	float B;
	float C;
	float D;
	float E;
	float F;
} calibration_matrix_t;

typedef enum
{
	CIRCLE,
	RECTANGLE
} shape_t;

typedef enum
{
	INIT,
	WAIT_FOR_P1,
	WAIT_FOR_P2,
	WAIT_FOR_P3
} calib_state_t;

typedef enum
{
	SHORT_PRESS,
	LONG_PRESS,
	HOLD
} click_t;

#define X_PIXELS (uint16_t)320
#define Y_PIXELS (uint16_t)240

// define max RGB values (green limited to 62 due to slider compatibility)
#define RED_MAX 31
#define GREEN_MAX 62
#define BLUE_MAX 31

// define slider's marker width and height
#define SLIDER_W 3
#define SLIDER_H 20

// define slider's length and offset which helps in reaching the last value
#define SLIDER_LENGTH 248
#define SLIDER_OFFSET 4

#define VALID 0UL
#define ERROR 1UL
#define NOTOUCH 2UL
#define COUNTING 3UL
#define CLICK 4UL

#define TS_INT IORD_ALTERA_AVALON_PIO_DATA(LCD_TS_INT_BASE)
#define KEY1_MASK (0x1)
#define KEY2_MASK (0x2)
#define KEY3_MASK (0x4)

#define COLOR_BG 0xFF0F
#define COLOR_MAIN 0x000F
#define COLOR_BLACK 0x0000
#define COLOR_WHITE 0xFFFF
#define COLOR_RED 0xFC00
#define COLOR_GREEN 0x3C0
#define COLOR_BLUE 0x003F

#endif /* SRC_TYPEDEFS_H_ */

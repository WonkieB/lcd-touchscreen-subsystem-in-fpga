/*
 * system.h
 *
 *  Created on: 16 dec 2020
 *      Author: Lukasz Bielecki
 */

#ifndef SRC_GUI_SYSTEM_H_
#define SRC_GUI_SYSTEM_H_

#include "display.h"
#include "touchscreen.h"
#include "calibration.h"
#include "typedefs.h"
#include "sys/alt_sys_wrappers.h"
#include "sys/alt_stdio.h"
#include <stdio.h>
#include <string.h>

void init_gui(void);
void add_basic_elem(basic_menu_elem_t* element, top_menu_elem_t* top);
uint8_t delete_basic_elem(size_t element, top_menu_elem_t* top);
void add_top_elem(top_menu_elem_t* element);
uint8_t delete_top_elem(void);
size_t draw_menu(top_menu_elem_t* top);
void erase_menu (void);

void draw_button(basic_menu_elem_t* element, top_menu_elem_t* top);
void focus_button(basic_menu_elem_t* element, top_menu_elem_t* top);
void activate_color(basic_menu_elem_t* element, top_menu_elem_t* top);
void activate_shape(basic_menu_elem_t* element, top_menu_elem_t* top);
void deactivate_color(basic_menu_elem_t* element, top_menu_elem_t* top);
void deactivate_shape(basic_menu_elem_t* element, top_menu_elem_t* top);

void draw_list_elem(basic_menu_elem_t* element, top_menu_elem_t* top);
void focus_list_elem(basic_menu_elem_t* element, top_menu_elem_t* top);

void draw_fill_checkbox(basic_menu_elem_t* element, top_menu_elem_t* top);
void click_fill_checkbox(basic_menu_elem_t* element, top_menu_elem_t* top);

void draw_slider_elem(basic_menu_elem_t* element, top_menu_elem_t* top);
void focus_slider_elem(basic_menu_elem_t* element, top_menu_elem_t* top, pixel_t* position);
void draw_color_probe(basic_menu_elem_t* element, top_menu_elem_t* top);

uint8_t is_point_within(pixel_t* point, pixel_t* start_point, uint16_t width, uint16_t height);
void process_touch(pixel_t* touch_coords, uint8_t* touch_status);
uint8_t is_menu_active(void);
uint16_t merge_rgb(uint8_t red, uint8_t green, uint8_t blue);
uint16_t map_rgb_to_slider(uint8_t color, uint8_t color_max, uint16_t slider_length);
uint8_t map_slider_to_rgb(uint16_t position, uint8_t color_max, uint16_t slider_length);

#endif /* SRC_GUI_SYSTEM_H_ */

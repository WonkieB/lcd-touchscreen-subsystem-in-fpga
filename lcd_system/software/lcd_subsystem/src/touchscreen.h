/*
 * touchscreen.h
 *
 *  Created on: 9 dec 2020
 *      Author: �ukasz Bielecki
 */

#ifndef SRC_TOUCHSCREEN_H_
#define SRC_TOUCHSCREEN_H_

#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include "system.h"
#include "altera_avalon_spi.h"
#include "sys/alt_sys_wrappers.h"
#include "altera_avalon_pio_regs.h"
#include "altera_avalon_timer_regs.h"
#include "altera_avalon_timer.h"
#include "sys/alt_irq.h"
#include "typedefs.h"

#define READ_TS_X 0x0090
#define READ_TS_Y 0x00D0
#define TS_VALID 0
#define TS_ERROR 1
#define TS_NOTOUCH 2

#define FILTER_SHIFT 2UL
#define MAX_OFFSET 2

uint8_t read_ts(pixel_t* coords_read);
void filter_data(uint16_t* input, uint16_t* output, uint32_t* filter_reg);

#endif /* SRC_TOUCHSCREEN_H_ */

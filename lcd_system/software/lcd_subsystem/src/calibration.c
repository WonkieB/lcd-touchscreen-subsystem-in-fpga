/*
 * calibration.c
 *
 *  Created on: 21 gru 2020
 *      Author: Wonkie
 */

#include "calibration.h"

// default calibration values set experimentally
calibration_matrix_t ts_calib = {0.09124663702, -0.00004219497666, -33.75294027, 0.002953648366, -0.0693655277, 254.0056852};
pixel_t ref_point3 = {20, 120};
pixel_t ref_point2 = {160, 20};
pixel_t ref_point1 = {300, 220};

void calibrate_ts(void)
{
	static calib_state_t c_state = INIT;
	static pixel_t point1;
	static pixel_t point2;
	static pixel_t point3;
	char output_msg[200];

	alt_irq_context irq_context = alt_irq_disable_all();

	draw_cross(&ref_point1, 10, COLOR_BLACK);
	while(read_ts(&point1))ALT_USLEEP(10000);	// wait for touch input
	draw_cross(&ref_point1, 10, COLOR_BG);	// remove the cross
	while(!TS_INT);	// wait for touch release
	ALT_USLEEP(10000);

	draw_cross(&ref_point2, 10, COLOR_BLACK);
	while(read_ts(&point2))ALT_USLEEP(10000);	// wait for touch input
	draw_cross(&ref_point2, 10, COLOR_BG);	// remove the cross
	while(!TS_INT);	// wait for touch release
	ALT_USLEEP(10000);

	draw_cross(&ref_point3, 10, COLOR_BLACK);
	while(read_ts(&point3))ALT_USLEEP(10000);	// wait for touch input
	draw_cross(&ref_point3, 10, COLOR_BG);	// remove the cross
	while(!TS_INT);	// wait for touch release

	set_calibration_matrix(&point1, &point2, &point3);
	alt_irq_enable_all(irq_context);

	sprintf(output_msg,"Calib 1 X coord: %i, \tY coord: %i\n", (point1.X), (point1.Y));
	alt_printf(output_msg);
	sprintf(output_msg,"Calib 2 X coord: %i, \tY coord: %i\n", (point2.X), (point2.Y));
	alt_printf(output_msg);
	sprintf(output_msg,"Calib 3 X coord: %i, \tY coord: %i\n", (point3.X), (point3.Y));
	alt_printf(output_msg);
}

void set_calibration_matrix(const pixel_t* point1, const pixel_t* point2, const pixel_t* point3)
{
	int16_t X0 = point1->X, Y0 = point1->Y;
	int16_t X1 = point2->X, Y1 = point2->Y;
	int16_t X2 = point3->X, Y2 = point3->Y;
	int16_t XD0 = ref_point1.X, YD0 = ref_point1.Y;
	int16_t XD1 = ref_point2.X, YD1 = ref_point2.Y;
	int16_t XD2 = ref_point3.X, YD2 = ref_point3.Y;

	float denominator = (((X0 - X2)*(Y1 - Y2)) -
			((X1 - X2)*(Y0 - Y2)));

	ts_calib.A = (((XD0 - XD2)*(Y1 - Y2)) -
			((XD1 - XD2)*(Y0 - Y2)));
	ts_calib.A /= denominator;

	ts_calib.B = (((X0 - X2)*(XD1 - XD2)) -
			((XD0 - XD2)*(X1 - X2)));
	ts_calib.B /= denominator;

	ts_calib.C = (Y0 * ((X2 * XD1) - (X1 * XD2)) +
			Y1 * ((X0 * XD2) - (X2 * XD0)) +
			Y2 * ((X1 * XD0) - (X0 * XD1)));
	ts_calib.C /= denominator;

	ts_calib.D = (((YD0 - YD2)*(Y1 - Y2)) -
			((YD1 - YD2)*(Y0 - Y2)));
	ts_calib.D /= denominator;

	ts_calib.E = (((X0 - X2)*(YD1 - YD2)) -
			((YD0 - YD2)*(X1 - X2)));
	ts_calib.E /= denominator;

	ts_calib.F = (Y0 * ((X2 * YD1) - (X1 * YD2)) +
				Y1 * ((X0 * YD2) - (X2 * YD0)) +
				Y2 * ((X1 * YD0) - (X0 * YD1)));
	ts_calib.F /= denominator;
}

uint8_t read_ts_calib(pixel_t* coords_read)
{
	pixel_t temp_pixel;
	static pixel_t  aver_pixel, previous_pixel;
	uint8_t read_status = read_ts(&temp_pixel);
	static uint8_t sample_count, last_read_status, debouncing_counter;

	if(!read_status)	// proceed only for successful read
	{
		temp_pixel.X = (ts_calib.A * temp_pixel.X +ts_calib.B * temp_pixel.Y + ts_calib.C);
		if(temp_pixel.X < 0)
		{
			temp_pixel.X = 0;
		}
		else if(temp_pixel.X > X_PIXELS-1)
		{
			temp_pixel.X = X_PIXELS-1;
		}
		temp_pixel.Y = (ts_calib.D * temp_pixel.X +ts_calib.E * temp_pixel.Y + ts_calib.F);
		if(temp_pixel.Y < 0)
		{
			temp_pixel.Y = 0;
		}
		else if(temp_pixel.Y > Y_PIXELS-1)
		{
			temp_pixel.Y = Y_PIXELS-1;
		}

		if(!((abs(temp_pixel.X - previous_pixel.X) < MAX_PIXEL_OFFSET) && (abs(temp_pixel.Y - previous_pixel.Y) < MAX_PIXEL_OFFSET)))
		{
			++sample_count;
			aver_pixel.X += temp_pixel.X;
			aver_pixel.Y += temp_pixel.Y;
			// drop first measurements for better accuracy
	//		if(NOTOUCH == last_read_status)
	//		{
	//			++debouncing_counter;
	//			if(DEBOUNCING_SAMPLES == debouncing_counter)
	//			{
	//				debouncing_counter = 0;
	//				last_read_status = read_status;
	//			}
	//		}
			if(NUM_OF_SAMPLES == sample_count)
			{
				sample_count = 0;
				coords_read->X = aver_pixel.X/NUM_OF_SAMPLES;
				coords_read->Y = aver_pixel.Y/NUM_OF_SAMPLES;
				aver_pixel.X = 0;
				aver_pixel.Y = 0;
				read_status = VALID;
			}
			else
			{
				read_status = COUNTING;
			}
		}
		previous_pixel.X += temp_pixel.X;
		previous_pixel.Y += temp_pixel.Y;
	}
	// indicate click if the touchpanel is released
	else if(((NOTOUCH != last_read_status && CLICK != last_read_status)) && (NOTOUCH == read_status))
	{
		read_status = CLICK;
	}
	last_read_status = read_status;
	return read_status;
}

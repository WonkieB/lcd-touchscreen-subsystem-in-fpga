/*
 * calibration.h
 *
 *  Created on: 21 gru 2020
 *      Author: Wonkie
 */

#ifndef SRC_CALIBRATION_H_
#define SRC_CALIBRATION_H_

#include "display.h"
#include "touchscreen.h"
#include "typedefs.h"
#include "sys/alt_sys_wrappers.h"
#include "sys/alt_stdio.h"
#include <stdio.h>

#define NUM_OF_SAMPLES 10
#define DEBOUNCING_SAMPLES 1
#define MAX_PIXEL_OFFSET 4

void calibrate_ts(void);
void set_calibration_matrix(const pixel_t* point1, const pixel_t* point2, const pixel_t* point3);
uint8_t read_ts_calib(pixel_t* coords_read);

#endif /* SRC_CALIBRATION_H_ */

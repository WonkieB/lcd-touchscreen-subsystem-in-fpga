/*
 * display.c
 *
 *  Created on: 9 dec 2020
 *      Author: �ukasz Bielecki
 */

#include "display.h"
#include "OpenSans_Bold_12.c"

static uint16_t lcd_send[1];
uint8_t row_column_exchange = ERROR;

void lcd_write_command(uint8_t data)
{
	IOWR_ALTERA_AVALON_PIO_CLEAR_BITS(LCD_RS_BASE, 0x01);   // set DC line low
	lcd_send[0] = data;
	alt_irq_context irq_context = alt_irq_disable_all();
	alt_avalon_spi_command(SPI_BASE, 1, 1, lcd_send, 0, NULL, 0);		// send data to transmit buffer
	alt_irq_enable_all(irq_context);
}

void lcd_write_data(uint8_t data)
{
	IOWR_ALTERA_AVALON_PIO_SET_BITS(LCD_RS_BASE, 0x01);   // set DC line high
	lcd_send[0] = (data << 8);
	alt_irq_context irq_context = alt_irq_disable_all();
	alt_avalon_spi_command(SPI_BASE, 1, 1, lcd_send, 0, NULL, 0);
	alt_irq_enable_all(irq_context);
}

void lcd_write_pixel(uint16_t data)
{
	IOWR_ALTERA_AVALON_PIO_SET_BITS(LCD_RS_BASE, 0x01);   // set DC line high
	lcd_send[0] = data;
	alt_irq_context irq_context = alt_irq_disable_all();
	alt_avalon_spi_command(SPI_BASE, 1, 1, lcd_send, 0, NULL, 0);
	alt_irq_enable_all(irq_context);
}

void init_lcd(void)
{

	IOWR_ALTERA_AVALON_PIO_CLEAR_BITS(LCD_RESET_BASE, 0x01);	// hardware reset
	ALT_USLEEP(200000);
	IOWR_ALTERA_AVALON_PIO_SET_BITS(LCD_RESET_BASE, 0x01);
	ALT_USLEEP(200000);

	lcd_write_command(0x01);         		// software reset
	ALT_USLEEP(10000);
	lcd_write_command(0x36);         		// memory access control
	lcd_write_data(0x88);					// set RGB
	lcd_write_command(0x3A);         		// pixel format command
	lcd_write_data(0x55);					// set 565 color depth
	lcd_write_command(0x11);         		// sleep out
	ALT_USLEEP(250000);            			// delay to allow all changes to take effect
	lcd_write_command(0x2C);                // send pixel values
	for (size_t i=0; i<76800; ++i)               	// fill the screen with orange
	{
		lcd_write_pixel(COLOR_BG); 				// 0xFF0F - orange
	}
	lcd_write_command(0x29);         		// send Display ON command
}

uint8_t set_pixel_addr(const pixel_t* start_point, const pixel_t* end_point)
{

	if (ERROR == row_column_exchange)
	{
		if(start_point->X > end_point->X || start_point->Y > end_point->Y
				|| end_point->X > X_PIXELS-1 || end_point->Y > Y_PIXELS-1 || start_point->X < 0 || start_point->Y < 0)
		{
			return ERROR;
		}
	}
	else if(VALID == row_column_exchange)
	{
		if(start_point->X > end_point->X || start_point->Y > end_point->Y
				|| end_point->X > Y_PIXELS-1 || end_point->Y > X_PIXELS-1 || start_point->X < 0 || start_point->Y < 0)
			{
				return ERROR;
			}
	}


	lcd_write_command(0x2A);                // set column address
	lcd_write_pixel(start_point->Y);
	lcd_write_pixel(end_point->Y);
	lcd_write_command(0x2B);                // set page address
	lcd_write_pixel(start_point->X);
	lcd_write_pixel(end_point->X);
	lcd_write_command(0x2C);                // send pixel values
	return VALID;
}

uint8_t draw_pixel(const pixel_t* point, uint16_t color)
{
	if (ERROR == set_pixel_addr(point, point))
	{
		return ERROR;
	}
	lcd_write_pixel(color);
	return VALID;
}

uint8_t draw_h_line(const pixel_t* start_point, uint16_t length, uint16_t color)
{
	// TODO input check
	pixel_t end_point = *start_point;
	end_point.X += length-1;
	if (ERROR == set_pixel_addr(start_point, &end_point))
	{
		return ERROR;
	}
	for(size_t i=0; i<(length); ++i)
		lcd_write_pixel(color);
	return VALID;
}

uint8_t draw_v_line(const pixel_t* start_point, uint16_t length, uint16_t color)
{
	// TODO input check
	pixel_t end_point = *start_point;
	end_point.Y += length-1;
	if (ERROR == set_pixel_addr(start_point, &end_point))
	{
		return ERROR;
	};
	for(size_t i=0; i<(length); ++i)
		lcd_write_pixel(color);
	return VALID;
}

uint8_t draw_cross(const pixel_t* center_point, uint16_t segment_length, uint16_t color)
{
	// TODO input check
	pixel_t h_point = *center_point;
	h_point.X -= segment_length;
	pixel_t v_point = *center_point;
	v_point.Y -= segment_length;

	if((ERROR == draw_h_line(&h_point, (segment_length*2)+1, color)) || (ERROR == draw_v_line(&v_point, (segment_length*2)+1, color)))
	{
		return ERROR;
	}
	return VALID;
}

uint8_t draw_rectangle(const pixel_t* start_point, uint16_t width, uint16_t height, uint16_t color)
{
	// TODO input check
	pixel_t draw_point = *start_point;

	if((ERROR == draw_h_line(&draw_point, width, color)) || (ERROR == draw_v_line(&draw_point, height, color)))
	{
		return ERROR;
	}
	draw_point.X += width-1;
	if(ERROR == draw_v_line(&draw_point, height, color))
	{
		return ERROR;
	}
	draw_point.X = start_point->X;
	draw_point.Y += height-1;
	if(ERROR == draw_h_line(&draw_point, width, color))
	{
		return ERROR;
	}
	return VALID;
}

uint8_t draw_filled_rectangle(const pixel_t* start_point, uint16_t width, uint16_t height, uint16_t color)
{
	// TODO input check
	pixel_t end_point = *start_point;
	end_point.X += width-1;
	end_point.Y += height-1;

	if (ERROR == set_pixel_addr(start_point, &end_point))
	{
		return ERROR;
	}
	for(size_t i=0; i<(width*height); ++i)
		lcd_write_pixel(color);
	return VALID;
}

void draw_framed_rectangle(const pixel_t* start_point, uint16_t width, uint16_t height, uint16_t color_border, uint16_t color_main)
{
    pixel_t draw_point = *start_point;
    draw_rectangle(&draw_point, width, height, color_border);
    draw_point.X += 1;	// move drawing point due to the borders
    draw_point.Y += 1;
    draw_filled_rectangle(&draw_point, width-2, height-2, color_main);
}

/*--------------------------------------------------------------------------------------------------------
 * The following functions come from Marek Ryn's repository: https://github.com/MarekRyn/ILI9341-Toolkit
  --------------------------------------------------------------------------------------------------------*/

uint16_t sinus[] = {0, 4, 8, 13, 17, 22, 26, 31, 35, 40, 44, 49, 53, 57, 62, 66,
		71, 75, 80, 84, 88, 93, 97, 102, 106, 110, 115, 119, 123, 128, 132, 136,
		141, 145, 149, 153, 158, 162, 166, 170, 175, 179, 183, 187, 191, 195, 200, 204,
		208, 212, 216, 220, 224, 228, 232, 236, 240, 244, 248, 252, 256, 259, 263, 267,
		271, 275, 278, 282, 286, 289, 293, 297, 300, 304, 308, 311, 315, 318, 322, 325,
		329, 332, 335, 339, 342, 345, 349, 352, 355, 358, 362, 365, 368, 371, 374, 377,
		380, 383, 386, 389, 392, 395, 397, 400, 403, 406, 408, 411, 414, 416, 419, 421,
		424, 426, 429, 431, 434, 436, 438, 441, 443, 445, 447, 449, 452, 454, 456, 458,
		460, 462, 464, 465, 467, 469, 471, 473, 474, 476, 477, 479, 481, 482, 484, 485,
		486, 488, 489, 490, 492, 493, 494, 495, 496, 497, 498, 499, 500, 501, 502, 503,
		504, 504, 505, 506, 507, 507, 508, 508, 509, 509, 510, 510, 510, 511, 511, 511,
		511, 511, 511, 511, 512};

uint8_t draw_circle(const pixel_t* start_point, uint16_t r, uint16_t color) {
	//TODO: Input Check

	int16_t x, y, a;
	pixel_t draw_point0, draw_point1, draw_point2, draw_point3;
	for (a=0; a<180; a++) {
		x = (sinus[a] * r) >> 9;
		y = (sinus[180-a] * r) >> 9;
		draw_point0.X = start_point->X + x;
		draw_point0.Y = start_point->Y + y;
		draw_point1.X = start_point->X + x;
		draw_point1.Y = start_point->Y - y;
		draw_point2.X = start_point->X - x;
		draw_point2.Y = start_point->Y + y;
		draw_point3.X = start_point->X - x;
		draw_point3.Y = start_point->Y - y;

		draw_pixel(&draw_point0, color);
		draw_pixel(&draw_point1, color);
		draw_pixel(&draw_point2, color);
		draw_pixel(&draw_point3, color);
	}
	return VALID;
}

uint8_t draw_filled_circle(const pixel_t* start_point, uint16_t r, uint16_t color) {
	//TODO: Input Check

	int16_t x, y, a;
	pixel_t draw_point0, draw_point1;

	for (a=0; a<180; a++) {

		x = (sinus[a] * r) >> 9;
		y = (sinus[180-a] * r) >> 9;
		draw_point0.X = start_point->X - x;
		draw_point0.Y = start_point->Y + y;
		draw_point1.X = start_point->X - x;
		draw_point1.Y = start_point->Y - y;

		draw_h_line(&draw_point0, 2 * x, color);
		draw_h_line(&draw_point1, 2 * x, color);
	}
	return VALID;
}

void draw_bg(void)
{
	pixel_t zero_point = {0,0};
	draw_filled_rectangle(&zero_point, X_PIXELS, Y_PIXELS, COLOR_BG);
}

uint16_t mixcolors(uint16_t color0, uint16_t color1, uint8_t mix) {

	uint32_t r0 = (color0 >> 8) & 0x00F8;
	uint32_t g0 = (color0 >> 3) & 0x00FC;
	uint32_t b0 = (color0 << 3) & 0x00F8;

	uint32_t r1 = (color1 >> 8) & 0x00F8;
	uint32_t g1 = (color1 >> 3) & 0x00FC;
	uint32_t b1 = (color1 << 3) & 0x00F8;

	uint32_t r = (((16 - mix) * r0) + (mix * r1)) >> 4;
	uint32_t g = (((16 - mix) * g0) + (mix * g1)) >> 4;
	uint32_t b = (((16 - mix) * b0) + (mix * b1)) >> 4;

	uint16_t rr = (r & 0xF8) << 8;
	uint16_t gg = (g & 0xFC) << 3;
	uint16_t bb = (b >> 3);

	return (rr | gg | bb);
}

uint16_t character(pixel_t* start_point, uint8_t font_data[], uint16_t font_color, uint16_t bg_color, char ch) {

	uint16_t x = start_point->X;
	uint16_t y = start_point->Y;
	pixel_t my_start_point = {y,x};

	uint16_t colors[4] = {mixcolors(bg_color, font_color, 0),
						  mixcolors(bg_color, font_color, 5),
						  mixcolors(bg_color, font_color, 10),
						  mixcolors(bg_color, font_color, 16)};
	uint16_t *adr0;
	uint16_t *adr1;
	uint8_t m;
	uint8_t i;
	uint8_t height = font_data[0];
	uint8_t swidth = font_data[1];

	// Character outside allowable range
	if ((ch < 33) || (ch > 126)) {
		draw_filled_rectangle(&my_start_point, height, swidth, colors[0]);
		return (x + swidth);
	}

	uint16_t a = (ch - 33) * 2;
	adr0 = (uint16_t*)&font_data[a+2];
	adr1 = (uint16_t*)&font_data[a+4];

	// Character outside defined subset
	if (*adr0 == *adr1) {
		draw_filled_rectangle(&my_start_point, height, swidth, colors[0]);
		return (x + swidth);
	}

	uint8_t width = font_data[*adr0];


	pixel_t font_point = {y + height - 1, x + width - 1  };
	set_pixel_addr(&my_start_point, &font_point);

	for (uint16_t j = *adr0+1; j<*adr1; j++) {
		m = font_data[j] >> 6;
		switch(m) {

			case 0:
			case 3:
				for (i=0; i<(font_data[j] & 0x3F); i++)
				{
					lcd_write_pixel(colors[m]);
				}
				break;

			case 1:
			case 2:
				lcd_write_pixel(colors[font_data[j] >> 6]);
				lcd_write_pixel(colors[(font_data[j] >> 4) & 0x03]);
				lcd_write_pixel(colors[(font_data[j] >> 2) & 0x03]);
				lcd_write_pixel(colors[font_data[j] & 0x03]);
				break;
		}
	}
	font_point.Y = x + width;
	font_point.X = y;
	draw_filled_rectangle(&font_point, height, (swidth / 8), colors[0]);
	return (x + width + (swidth / 8));
}

// Displays text with encoded font
//(for encoding image use python script attached to this repository)
uint16_t TFT_Text(pixel_t* start_point, uint16_t font_color, uint16_t bg_color, char *str) {
	pixel_t letter_point;
	letter_point.X = start_point->X;
	letter_point.Y = Y_PIXELS - start_point->Y;
	lcd_write_command(0x36);         		// memory access control
	lcd_write_data(0xE8);					// set RGB
	row_column_exchange = VALID;
	while (*str > 0) {

		letter_point.X = character(&letter_point, open_sans_12, font_color, bg_color, *str);
		str += 1;
	}
	lcd_write_command(0x36);         		// memory access control
	lcd_write_data(0x88);					// set RGB
	row_column_exchange = ERROR;
	return letter_point.X;
}

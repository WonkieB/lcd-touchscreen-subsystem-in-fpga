/*
 * display.h
 *
 *  Created on: 9 dec 2020
 *      Author: �ukasz Bielecki
 */

#ifndef SRC_DISPLAY_H_
#define SRC_DISPLAY_H_

#include <stdint.h>
#include <stdlib.h>
#include "sys/alt_sys_wrappers.h"
#include "altera_avalon_pio_regs.h"
#include "altera_avalon_spi.h"
#include "system.h"
#include "sys/alt_irq.h"
#include "typedefs.h"



void lcd_write_command(uint8_t data);
void lcd_write_data(uint8_t data);
void lcd_write_pixel(uint16_t data);
void init_lcd(void);

uint8_t set_pixel_addr(const pixel_t* start_point, const pixel_t* end_point);

uint8_t draw_pixel(const pixel_t* point, uint16_t color);
uint8_t draw_h_line(const pixel_t* start_point, uint16_t length, uint16_t color);
uint8_t draw_v_line(const pixel_t* start_point, uint16_t length, uint16_t color);
uint8_t draw_cross(const pixel_t* center_point, uint16_t segment_length, uint16_t color);
uint8_t draw_rectangle(const pixel_t* start_point, uint16_t width, uint16_t height, uint16_t color);
uint8_t draw_filled_rectangle(const pixel_t* start_point, uint16_t width, uint16_t height, uint16_t color);
void draw_framed_rectangle(const pixel_t* start_point, uint16_t width, uint16_t height, uint16_t color_border, uint16_t color_main);
uint8_t draw_circle(const pixel_t* start_point, uint16_t r, uint16_t color);
uint8_t draw_filled_circle(const pixel_t* start_point, uint16_t r, uint16_t color);
void draw_bg(void);

uint16_t mixcolors(uint16_t color0, uint16_t color1, uint8_t mix);
uint16_t character(pixel_t* start_point, uint8_t font_data[], uint16_t font_color, uint16_t bg_color, char ch);
uint16_t TFT_Text(pixel_t* start_point, uint16_t font_color, uint16_t bg_color, char *str);

#endif /* SRC_DISPLAY_H_ */

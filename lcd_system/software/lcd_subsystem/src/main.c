/* 
 * FILENAME:	main.c
 * 
 * DESCRIPTION:
 * 		Main file of the LCD subsystem project.
 * 
 * FUNCTIONS:
 * 		int		main(void)
 * 		void 	lcd_write_command(uint8_t data)
 * 		void	lcd_write_data(uint8_t data)
 *		void	init_lcd(void)
 *		int16_t	read_ts(void
 *
 * 
 * AUTHOR:	�ukasz Bielecki			START DATE:	4 Nov 2020
*/

#include "main.h"


int main(void)
{

	volatile pixel_t ts_coords = {0, 0};
	pixel_t cross = {100, 50};

	volatile uint8_t new_data_ready = 0;
	volatile keys_t keys = {0b111,0b111};
	char output_msg[100];
	volatile void* ts_data[] = {&ts_coords, &new_data_ready};

	init_lcd();
	init_gui();

	IOWR_ALTERA_AVALON_PIO_EDGE_CAP(LCD_KEYS_BASE, 0b111);
	IOWR_ALTERA_AVALON_PIO_IRQ_MASK(LCD_KEYS_BASE, 0b111);
	alt_ic_isr_register(LCD_KEYS_IRQ_INTERRUPT_CONTROLLER_ID, LCD_KEYS_IRQ, keys_interrupt, &keys, NULL);
	alt_ic_isr_register(DEBOUNCER_TIMER_IRQ_INTERRUPT_CONTROLLER_ID, DEBOUNCER_TIMER_IRQ, debouncer_timer_interrupt, &keys, NULL);
	alt_ic_isr_register(TS_TIMER_IRQ_INTERRUPT_CONTROLLER_ID, TS_TIMER_IRQ, ts_timer_interrupt, ts_data, NULL);
	IOWR_ALTERA_AVALON_TIMER_CONTROL(TS_TIMER_BASE, ALTERA_AVALON_TIMER_CONTROL_START_MSK |
			ALTERA_AVALON_TIMER_CONTROL_CONT_MSK | ALTERA_AVALON_TIMER_CONTROL_ITO_MSK);

	//draw_rectangle(&cross,20,10,COLOR_BLACK);
	//draw_cross(&cross, 10, COLOR_BLACK);
	//calibrate_ts();
	while (1){
		// read from touchscreen
		if(!(keys.pressed & KEY1_MASK))
		{
			draw_bg();
			calibrate_ts();
			keys.pressed = 0b111;
		}
		else if(!(keys.pressed & KEY2_MASK))
		{
			draw_bg();
			keys.pressed = 0b111;
		}
		else if(!(keys.pressed & KEY3_MASK))
		{
			if(ERROR == is_menu_active())
			{
				draw_menu(NULL);
			}
			else if(VALID == is_menu_active())
			{
				erase_menu();
			}

			keys.pressed = 0b111;
		}
		alt_irq_context irq_context = alt_irq_disable_all();
		//new_data = !read_ts_calib(&ts_coords);
		process_touch(&ts_coords ,&new_data_ready);
		if(CLICK == new_data_ready)
		{
//			if(VALID == is_menu_active())
//			{
//				process_touch(&ts_coords ,&new_data_ready);
//			}
			sprintf(output_msg,"X coord: %i, \tY coord: %i\n", (ts_coords.X), (ts_coords.Y));
			alt_printf(output_msg);
			new_data_ready = ERROR;
		}
		else if(VALID == new_data_ready)
		{
//			if(VALID == is_menu_active())
//			{
//				process_touch(&ts_coords ,&new_data_ready);
//			}
//			else
//			{
//
//				draw_filled_rectangle(&ts_coords, 2, 2, COLOR_RED);
//				//draw_filled_circle(&ts_coords, 10, COLOR_RED);
//
//			}

			new_data_ready = ERROR;
		}
		new_data_ready = ERROR;
		alt_irq_enable_all(irq_context);
		//sprintf(output_msg,"X coord: %i, \tY coord: %i\n", (ts_coords.X), (ts_coords.Y));
		//alt_printf(output_msg);
		//ALT_USLEEP(1000);
	}
	return 0;
}

void keys_interrupt(void* context)
{
	// clear edge register
	IOWR_ALTERA_AVALON_PIO_EDGE_CAP(LCD_KEYS_BASE, 0b111);
	// read buttons
	((keys_t*) context)->pre_debounce = IORD_ALTERA_AVALON_PIO_DATA(LCD_KEYS_BASE);
	// reset and start debouncer timer
	reset_timer(DEBOUNCER_TIMER_BASE);
	IOWR_ALTERA_AVALON_TIMER_CONTROL(DEBOUNCER_TIMER_BASE, ALTERA_AVALON_TIMER_CONTROL_START_MSK | ALTERA_AVALON_TIMER_CONTROL_ITO_MSK);

}

void debouncer_timer_interrupt(void* context)
{
	// clear status register
	IOWR_ALTERA_AVALON_TIMER_STATUS(DEBOUNCER_TIMER_BASE, 0);
	// check if buttons state changed
	if( (((keys_t*) context)->pre_debounce) == IORD_ALTERA_AVALON_PIO_DATA(LCD_KEYS_BASE))
	{
		((keys_t*) context)->pressed = ((keys_t*) context)->pre_debounce;
	}
}

void ts_timer_interrupt(void* context[])
{
	uint8_t read_status;
	// clear interrupt status
	IOWR_ALTERA_AVALON_TIMER_STATUS(TS_TIMER_BASE, 0);
	alt_irq_context irq_context = alt_irq_disable_all();
	read_status = read_ts_calib(*context);
	// proceed only if latest data was processed
	if(ERROR == *((uint8_t*)*(context+1)))
	{
		// touchscreen is touched
		if(VALID == read_status)
		{
			*((uint8_t*)*(context+1)) = VALID;
		}
		// touchscreen is released after touch
		else if(CLICK == read_status)
		{
			*((uint8_t*)*(context+1)) = CLICK;
		}
	}
	alt_irq_enable_all(irq_context);
}

void reset_timer(uint8_t* base)
{
	// stop timer
	IOWR_ALTERA_AVALON_TIMER_CONTROL(base, ALTERA_AVALON_TIMER_CONTROL_STOP_MSK);

	// write to period register resets timer's value
	IOWR_ALTERA_AVALON_TIMER_PERIODL(base, IORD_ALTERA_AVALON_TIMER_PERIODL(base));
}

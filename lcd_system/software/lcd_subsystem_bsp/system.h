/*
 * system.h - SOPC Builder system and BSP software package information
 *
 * Machine generated for CPU 'CPU' in SOPC Builder design 'lcd_system'
 * SOPC Builder design path: D:/Rozne/AGieHa/Praca_inzynierska/lcd_system/lcd_system.sopcinfo
 *
 * Generated: Sun Dec 20 14:25:05 CET 2020
 */

/*
 * DO NOT MODIFY THIS FILE
 *
 * Changing this file will have subtle consequences
 * which will almost certainly lead to a nonfunctioning
 * system. If you do modify this file, be aware that your
 * changes will be overwritten and lost when this file
 * is generated again.
 *
 * DO NOT MODIFY THIS FILE
 */

/*
 * License Agreement
 *
 * Copyright (c) 2008
 * Altera Corporation, San Jose, California, USA.
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * This agreement shall be governed in all respects by the laws of the State
 * of California and by the laws of the United States of America.
 */

#ifndef __SYSTEM_H_
#define __SYSTEM_H_

/* Include definitions from linker script generator */
#include "linker.h"


/*
 * CPU configuration
 *
 */

#define ALT_CPU_ARCHITECTURE "altera_nios2_gen2"
#define ALT_CPU_BIG_ENDIAN 0
#define ALT_CPU_BREAK_ADDR 0x00010820
#define ALT_CPU_CPU_ARCH_NIOS2_R1
#define ALT_CPU_CPU_FREQ 100000000u
#define ALT_CPU_CPU_ID_SIZE 1
#define ALT_CPU_CPU_ID_VALUE 0x00000000
#define ALT_CPU_CPU_IMPLEMENTATION "tiny"
#define ALT_CPU_DATA_ADDR_WIDTH 0x11
#define ALT_CPU_DCACHE_LINE_SIZE 0
#define ALT_CPU_DCACHE_LINE_SIZE_LOG2 0
#define ALT_CPU_DCACHE_SIZE 0
#define ALT_CPU_EXCEPTION_ADDR 0x00008020
#define ALT_CPU_FLASH_ACCELERATOR_LINES 0
#define ALT_CPU_FLASH_ACCELERATOR_LINE_SIZE 0
#define ALT_CPU_FLUSHDA_SUPPORTED
#define ALT_CPU_FREQ 100000000
#define ALT_CPU_HARDWARE_DIVIDE_PRESENT 0
#define ALT_CPU_HARDWARE_MULTIPLY_PRESENT 0
#define ALT_CPU_HARDWARE_MULX_PRESENT 0
#define ALT_CPU_HAS_DEBUG_CORE 1
#define ALT_CPU_HAS_DEBUG_STUB
#define ALT_CPU_HAS_ILLEGAL_INSTRUCTION_EXCEPTION
#define ALT_CPU_HAS_JMPI_INSTRUCTION
#define ALT_CPU_ICACHE_LINE_SIZE 0
#define ALT_CPU_ICACHE_LINE_SIZE_LOG2 0
#define ALT_CPU_ICACHE_SIZE 0
#define ALT_CPU_INST_ADDR_WIDTH 0x11
#define ALT_CPU_NAME "CPU"
#define ALT_CPU_OCI_VERSION 1
#define ALT_CPU_RESET_ADDR 0x00008000


/*
 * CPU configuration (with legacy prefix - don't use these anymore)
 *
 */

#define NIOS2_BIG_ENDIAN 0
#define NIOS2_BREAK_ADDR 0x00010820
#define NIOS2_CPU_ARCH_NIOS2_R1
#define NIOS2_CPU_FREQ 100000000u
#define NIOS2_CPU_ID_SIZE 1
#define NIOS2_CPU_ID_VALUE 0x00000000
#define NIOS2_CPU_IMPLEMENTATION "tiny"
#define NIOS2_DATA_ADDR_WIDTH 0x11
#define NIOS2_DCACHE_LINE_SIZE 0
#define NIOS2_DCACHE_LINE_SIZE_LOG2 0
#define NIOS2_DCACHE_SIZE 0
#define NIOS2_EXCEPTION_ADDR 0x00008020
#define NIOS2_FLASH_ACCELERATOR_LINES 0
#define NIOS2_FLASH_ACCELERATOR_LINE_SIZE 0
#define NIOS2_FLUSHDA_SUPPORTED
#define NIOS2_HARDWARE_DIVIDE_PRESENT 0
#define NIOS2_HARDWARE_MULTIPLY_PRESENT 0
#define NIOS2_HARDWARE_MULX_PRESENT 0
#define NIOS2_HAS_DEBUG_CORE 1
#define NIOS2_HAS_DEBUG_STUB
#define NIOS2_HAS_ILLEGAL_INSTRUCTION_EXCEPTION
#define NIOS2_HAS_JMPI_INSTRUCTION
#define NIOS2_ICACHE_LINE_SIZE 0
#define NIOS2_ICACHE_LINE_SIZE_LOG2 0
#define NIOS2_ICACHE_SIZE 0
#define NIOS2_INST_ADDR_WIDTH 0x11
#define NIOS2_OCI_VERSION 1
#define NIOS2_RESET_ADDR 0x00008000


/*
 * DEBOUNCER_TIMER configuration
 *
 */

#define ALT_MODULE_CLASS_DEBOUNCER_TIMER altera_avalon_timer
#define DEBOUNCER_TIMER_ALWAYS_RUN 0
#define DEBOUNCER_TIMER_BASE 0x11080
#define DEBOUNCER_TIMER_COUNTER_SIZE 32
#define DEBOUNCER_TIMER_FIXED_PERIOD 0
#define DEBOUNCER_TIMER_FREQ 100000000
#define DEBOUNCER_TIMER_IRQ 1
#define DEBOUNCER_TIMER_IRQ_INTERRUPT_CONTROLLER_ID 0
#define DEBOUNCER_TIMER_LOAD_VALUE 19999999
#define DEBOUNCER_TIMER_MULT 0.001
#define DEBOUNCER_TIMER_NAME "/dev/DEBOUNCER_TIMER"
#define DEBOUNCER_TIMER_PERIOD 200
#define DEBOUNCER_TIMER_PERIOD_UNITS "ms"
#define DEBOUNCER_TIMER_RESET_OUTPUT 0
#define DEBOUNCER_TIMER_SNAPSHOT 1
#define DEBOUNCER_TIMER_SPAN 32
#define DEBOUNCER_TIMER_TICKS_PER_SEC 5
#define DEBOUNCER_TIMER_TIMEOUT_PULSE_OUTPUT 0
#define DEBOUNCER_TIMER_TYPE "altera_avalon_timer"


/*
 * Define for each module class mastered by the CPU
 *
 */

#define __ALTERA_AVALON_JTAG_UART
#define __ALTERA_AVALON_ONCHIP_MEMORY2
#define __ALTERA_AVALON_PIO
#define __ALTERA_AVALON_SPI
#define __ALTERA_AVALON_SYSID_QSYS
#define __ALTERA_AVALON_TIMER
#define __ALTERA_NIOS2_GEN2
#define __ALTPLL


/*
 * JTAG_UART configuration
 *
 */

#define ALT_MODULE_CLASS_JTAG_UART altera_avalon_jtag_uart
#define JTAG_UART_BASE 0x110f8
#define JTAG_UART_IRQ 0
#define JTAG_UART_IRQ_INTERRUPT_CONTROLLER_ID 0
#define JTAG_UART_NAME "/dev/JTAG_UART"
#define JTAG_UART_READ_DEPTH 64
#define JTAG_UART_READ_THRESHOLD 8
#define JTAG_UART_SPAN 8
#define JTAG_UART_TYPE "altera_avalon_jtag_uart"
#define JTAG_UART_WRITE_DEPTH 64
#define JTAG_UART_WRITE_THRESHOLD 8


/*
 * LCD_KEYS configuration
 *
 */

#define ALT_MODULE_CLASS_LCD_KEYS altera_avalon_pio
#define LCD_KEYS_BASE 0x110d0
#define LCD_KEYS_BIT_CLEARING_EDGE_REGISTER 1
#define LCD_KEYS_BIT_MODIFYING_OUTPUT_REGISTER 0
#define LCD_KEYS_CAPTURE 1
#define LCD_KEYS_DATA_WIDTH 3
#define LCD_KEYS_DO_TEST_BENCH_WIRING 0
#define LCD_KEYS_DRIVEN_SIM_VALUE 0
#define LCD_KEYS_EDGE_TYPE "FALLING"
#define LCD_KEYS_FREQ 100000000
#define LCD_KEYS_HAS_IN 1
#define LCD_KEYS_HAS_OUT 0
#define LCD_KEYS_HAS_TRI 0
#define LCD_KEYS_IRQ 3
#define LCD_KEYS_IRQ_INTERRUPT_CONTROLLER_ID 0
#define LCD_KEYS_IRQ_TYPE "EDGE"
#define LCD_KEYS_NAME "/dev/LCD_KEYS"
#define LCD_KEYS_RESET_VALUE 0
#define LCD_KEYS_SPAN 16
#define LCD_KEYS_TYPE "altera_avalon_pio"


/*
 * LCD_RESET configuration
 *
 */

#define ALT_MODULE_CLASS_LCD_RESET altera_avalon_pio
#define LCD_RESET_BASE 0x11060
#define LCD_RESET_BIT_CLEARING_EDGE_REGISTER 0
#define LCD_RESET_BIT_MODIFYING_OUTPUT_REGISTER 1
#define LCD_RESET_CAPTURE 0
#define LCD_RESET_DATA_WIDTH 1
#define LCD_RESET_DO_TEST_BENCH_WIRING 0
#define LCD_RESET_DRIVEN_SIM_VALUE 0
#define LCD_RESET_EDGE_TYPE "NONE"
#define LCD_RESET_FREQ 100000000
#define LCD_RESET_HAS_IN 0
#define LCD_RESET_HAS_OUT 1
#define LCD_RESET_HAS_TRI 0
#define LCD_RESET_IRQ -1
#define LCD_RESET_IRQ_INTERRUPT_CONTROLLER_ID -1
#define LCD_RESET_IRQ_TYPE "NONE"
#define LCD_RESET_NAME "/dev/LCD_RESET"
#define LCD_RESET_RESET_VALUE 1
#define LCD_RESET_SPAN 32
#define LCD_RESET_TYPE "altera_avalon_pio"


/*
 * LCD_RS configuration
 *
 */

#define ALT_MODULE_CLASS_LCD_RS altera_avalon_pio
#define LCD_RS_BASE 0x110a0
#define LCD_RS_BIT_CLEARING_EDGE_REGISTER 0
#define LCD_RS_BIT_MODIFYING_OUTPUT_REGISTER 1
#define LCD_RS_CAPTURE 0
#define LCD_RS_DATA_WIDTH 1
#define LCD_RS_DO_TEST_BENCH_WIRING 0
#define LCD_RS_DRIVEN_SIM_VALUE 0
#define LCD_RS_EDGE_TYPE "NONE"
#define LCD_RS_FREQ 100000000
#define LCD_RS_HAS_IN 0
#define LCD_RS_HAS_OUT 1
#define LCD_RS_HAS_TRI 0
#define LCD_RS_IRQ -1
#define LCD_RS_IRQ_INTERRUPT_CONTROLLER_ID -1
#define LCD_RS_IRQ_TYPE "NONE"
#define LCD_RS_NAME "/dev/LCD_RS"
#define LCD_RS_RESET_VALUE 0
#define LCD_RS_SPAN 32
#define LCD_RS_TYPE "altera_avalon_pio"


/*
 * LCD_TS_INT configuration
 *
 */

#define ALT_MODULE_CLASS_LCD_TS_INT altera_avalon_pio
#define LCD_TS_INT_BASE 0x110c0
#define LCD_TS_INT_BIT_CLEARING_EDGE_REGISTER 1
#define LCD_TS_INT_BIT_MODIFYING_OUTPUT_REGISTER 0
#define LCD_TS_INT_CAPTURE 1
#define LCD_TS_INT_DATA_WIDTH 1
#define LCD_TS_INT_DO_TEST_BENCH_WIRING 0
#define LCD_TS_INT_DRIVEN_SIM_VALUE 0
#define LCD_TS_INT_EDGE_TYPE "ANY"
#define LCD_TS_INT_FREQ 100000000
#define LCD_TS_INT_HAS_IN 1
#define LCD_TS_INT_HAS_OUT 0
#define LCD_TS_INT_HAS_TRI 0
#define LCD_TS_INT_IRQ 4
#define LCD_TS_INT_IRQ_INTERRUPT_CONTROLLER_ID 0
#define LCD_TS_INT_IRQ_TYPE "EDGE"
#define LCD_TS_INT_NAME "/dev/LCD_TS_INT"
#define LCD_TS_INT_RESET_VALUE 0
#define LCD_TS_INT_SPAN 16
#define LCD_TS_INT_TYPE "altera_avalon_pio"


/*
 * PLL configuration
 *
 */

#define ALT_MODULE_CLASS_PLL altpll
#define PLL_BASE 0x110e0
#define PLL_IRQ -1
#define PLL_IRQ_INTERRUPT_CONTROLLER_ID -1
#define PLL_NAME "/dev/PLL"
#define PLL_SPAN 16
#define PLL_TYPE "altpll"


/*
 * RAM configuration
 *
 */

#define ALT_MODULE_CLASS_RAM altera_avalon_onchip_memory2
#define RAM_ALLOW_IN_SYSTEM_MEMORY_CONTENT_EDITOR 0
#define RAM_ALLOW_MRAM_SIM_CONTENTS_ONLY_FILE 0
#define RAM_BASE 0x8000
#define RAM_CONTENTS_INFO ""
#define RAM_DUAL_PORT 0
#define RAM_GUI_RAM_BLOCK_TYPE "AUTO"
#define RAM_INIT_CONTENTS_FILE "lcd_system_RAM"
#define RAM_INIT_MEM_CONTENT 0
#define RAM_INSTANCE_ID "NONE"
#define RAM_IRQ -1
#define RAM_IRQ_INTERRUPT_CONTROLLER_ID -1
#define RAM_NAME "/dev/RAM"
#define RAM_NON_DEFAULT_INIT_FILE_ENABLED 0
#define RAM_RAM_BLOCK_TYPE "AUTO"
#define RAM_READ_DURING_WRITE_MODE "DONT_CARE"
#define RAM_SINGLE_CLOCK_OP 0
#define RAM_SIZE_MULTIPLE 1
#define RAM_SIZE_VALUE 32768
#define RAM_SPAN 32768
#define RAM_TYPE "altera_avalon_onchip_memory2"
#define RAM_WRITABLE 1


/*
 * SPI configuration
 *
 */

#define ALT_MODULE_CLASS_SPI altera_avalon_spi
#define SPI_BASE 0x11040
#define SPI_CLOCKMULT 1
#define SPI_CLOCKPHASE 0
#define SPI_CLOCKPOLARITY 0
#define SPI_CLOCKUNITS "Hz"
#define SPI_DATABITS 16
#define SPI_DATAWIDTH 16
#define SPI_DELAYMULT "1.0E-9"
#define SPI_DELAYUNITS "ns"
#define SPI_EXTRADELAY 0
#define SPI_INSERT_SYNC 0
#define SPI_IRQ 2
#define SPI_IRQ_INTERRUPT_CONTROLLER_ID 0
#define SPI_ISMASTER 1
#define SPI_LSBFIRST 0
#define SPI_NAME "/dev/SPI"
#define SPI_NUMSLAVES 2
#define SPI_PREFIX "spi_"
#define SPI_SPAN 32
#define SPI_SYNC_REG_DEPTH 2
#define SPI_TARGETCLOCK 2500000u
#define SPI_TARGETSSDELAY "100.0"
#define SPI_TYPE "altera_avalon_spi"


/*
 * SYSID configuration
 *
 */

#define ALT_MODULE_CLASS_SYSID altera_avalon_sysid_qsys
#define SYSID_BASE 0x110f0
#define SYSID_ID 0
#define SYSID_IRQ -1
#define SYSID_IRQ_INTERRUPT_CONTROLLER_ID -1
#define SYSID_NAME "/dev/SYSID"
#define SYSID_SPAN 8
#define SYSID_TIMESTAMP 1608470257
#define SYSID_TYPE "altera_avalon_sysid_qsys"


/*
 * System configuration
 *
 */

#define ALT_DEVICE_FAMILY "MAX 10"
#define ALT_ENHANCED_INTERRUPT_API_PRESENT
#define ALT_IRQ_BASE NULL
#define ALT_LOG_PORT "/dev/null"
#define ALT_LOG_PORT_BASE 0x0
#define ALT_LOG_PORT_DEV null
#define ALT_LOG_PORT_TYPE ""
#define ALT_NUM_EXTERNAL_INTERRUPT_CONTROLLERS 0
#define ALT_NUM_INTERNAL_INTERRUPT_CONTROLLERS 1
#define ALT_NUM_INTERRUPT_CONTROLLERS 1
#define ALT_STDERR "/dev/JTAG_UART"
#define ALT_STDERR_BASE 0x110f8
#define ALT_STDERR_DEV JTAG_UART
#define ALT_STDERR_IS_JTAG_UART
#define ALT_STDERR_PRESENT
#define ALT_STDERR_TYPE "altera_avalon_jtag_uart"
#define ALT_STDIN "/dev/JTAG_UART"
#define ALT_STDIN_BASE 0x110f8
#define ALT_STDIN_DEV JTAG_UART
#define ALT_STDIN_IS_JTAG_UART
#define ALT_STDIN_PRESENT
#define ALT_STDIN_TYPE "altera_avalon_jtag_uart"
#define ALT_STDOUT "/dev/JTAG_UART"
#define ALT_STDOUT_BASE 0x110f8
#define ALT_STDOUT_DEV JTAG_UART
#define ALT_STDOUT_IS_JTAG_UART
#define ALT_STDOUT_PRESENT
#define ALT_STDOUT_TYPE "altera_avalon_jtag_uart"
#define ALT_SYSTEM_NAME "lcd_system"


/*
 * TS_TIMER configuration
 *
 */

#define ALT_MODULE_CLASS_TS_TIMER altera_avalon_timer
#define TS_TIMER_ALWAYS_RUN 0
#define TS_TIMER_BASE 0x11020
#define TS_TIMER_COUNTER_SIZE 32
#define TS_TIMER_FIXED_PERIOD 0
#define TS_TIMER_FREQ 100000000
#define TS_TIMER_IRQ 5
#define TS_TIMER_IRQ_INTERRUPT_CONTROLLER_ID 0
#define TS_TIMER_LOAD_VALUE 99999
#define TS_TIMER_MULT 0.001
#define TS_TIMER_NAME "/dev/TS_TIMER"
#define TS_TIMER_PERIOD 1
#define TS_TIMER_PERIOD_UNITS "ms"
#define TS_TIMER_RESET_OUTPUT 0
#define TS_TIMER_SNAPSHOT 1
#define TS_TIMER_SPAN 32
#define TS_TIMER_TICKS_PER_SEC 1000
#define TS_TIMER_TIMEOUT_PULSE_OUTPUT 0
#define TS_TIMER_TYPE "altera_avalon_timer"


/*
 * WATCHDOG configuration
 *
 */

#define ALT_MODULE_CLASS_WATCHDOG altera_avalon_timer
#define WATCHDOG_ALWAYS_RUN 1
#define WATCHDOG_BASE 0x11000
#define WATCHDOG_COUNTER_SIZE 32
#define WATCHDOG_FIXED_PERIOD 1
#define WATCHDOG_FREQ 100000000
#define WATCHDOG_IRQ 6
#define WATCHDOG_IRQ_INTERRUPT_CONTROLLER_ID 0
#define WATCHDOG_LOAD_VALUE 99999999
#define WATCHDOG_MULT 1.0
#define WATCHDOG_NAME "/dev/WATCHDOG"
#define WATCHDOG_PERIOD 1
#define WATCHDOG_PERIOD_UNITS "s"
#define WATCHDOG_RESET_OUTPUT 1
#define WATCHDOG_SNAPSHOT 1
#define WATCHDOG_SPAN 32
#define WATCHDOG_TICKS_PER_SEC 1
#define WATCHDOG_TIMEOUT_PULSE_OUTPUT 0
#define WATCHDOG_TYPE "altera_avalon_timer"


/*
 * hal configuration
 *
 */

#define ALT_INCLUDE_INSTRUCTION_RELATED_EXCEPTION_API
#define ALT_MAX_FD 4
#define ALT_SYS_CLK none
#define ALT_TIMESTAMP_CLK none

#endif /* __SYSTEM_H_ */

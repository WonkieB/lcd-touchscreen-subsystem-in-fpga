	component lcd_system is
		port (
			clk_clk           : in  std_logic                    := 'X';             -- clk
			lcd_keys_export   : in  std_logic_vector(2 downto 0) := (others => 'X'); -- export
			lcd_reset_export  : out std_logic;                                       -- export
			lcd_rs_export     : out std_logic;                                       -- export
			lcd_ts_int_export : in  std_logic                    := 'X';             -- export
			reset_reset_n     : in  std_logic                    := 'X';             -- reset_n
			spi_MISO          : in  std_logic                    := 'X';             -- MISO
			spi_MOSI          : out std_logic;                                       -- MOSI
			spi_SCLK          : out std_logic;                                       -- SCLK
			spi_SS_n          : out std_logic_vector(1 downto 0)                     -- SS_n
		);
	end component lcd_system;

	u0 : component lcd_system
		port map (
			clk_clk           => CONNECTED_TO_clk_clk,           --        clk.clk
			lcd_keys_export   => CONNECTED_TO_lcd_keys_export,   --   lcd_keys.export
			lcd_reset_export  => CONNECTED_TO_lcd_reset_export,  --  lcd_reset.export
			lcd_rs_export     => CONNECTED_TO_lcd_rs_export,     --     lcd_rs.export
			lcd_ts_int_export => CONNECTED_TO_lcd_ts_int_export, -- lcd_ts_int.export
			reset_reset_n     => CONNECTED_TO_reset_reset_n,     --      reset.reset_n
			spi_MISO          => CONNECTED_TO_spi_MISO,          --        spi.MISO
			spi_MOSI          => CONNECTED_TO_spi_MOSI,          --           .MOSI
			spi_SCLK          => CONNECTED_TO_spi_SCLK,          --           .SCLK
			spi_SS_n          => CONNECTED_TO_spi_SS_n           --           .SS_n
		);


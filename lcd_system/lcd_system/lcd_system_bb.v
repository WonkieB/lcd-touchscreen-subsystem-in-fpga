
module lcd_system (
	clk_clk,
	lcd_keys_export,
	lcd_reset_export,
	lcd_rs_export,
	lcd_ts_int_export,
	reset_reset_n,
	spi_MISO,
	spi_MOSI,
	spi_SCLK,
	spi_SS_n);	

	input		clk_clk;
	input	[2:0]	lcd_keys_export;
	output		lcd_reset_export;
	output		lcd_rs_export;
	input		lcd_ts_int_export;
	input		reset_reset_n;
	input		spi_MISO;
	output		spi_MOSI;
	output		spi_SCLK;
	output	[1:0]	spi_SS_n;
endmodule

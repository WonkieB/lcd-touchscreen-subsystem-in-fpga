	lcd_system u0 (
		.clk_clk           (<connected-to-clk_clk>),           //        clk.clk
		.lcd_keys_export   (<connected-to-lcd_keys_export>),   //   lcd_keys.export
		.lcd_reset_export  (<connected-to-lcd_reset_export>),  //  lcd_reset.export
		.lcd_rs_export     (<connected-to-lcd_rs_export>),     //     lcd_rs.export
		.lcd_ts_int_export (<connected-to-lcd_ts_int_export>), // lcd_ts_int.export
		.reset_reset_n     (<connected-to-reset_reset_n>),     //      reset.reset_n
		.spi_MISO          (<connected-to-spi_MISO>),          //        spi.MISO
		.spi_MOSI          (<connected-to-spi_MOSI>),          //           .MOSI
		.spi_SCLK          (<connected-to-spi_SCLK>),          //           .SCLK
		.spi_SS_n          (<connected-to-spi_SS_n>)           //           .SS_n
	);

